<?php
header("Content-Type: text/html;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
include '../includes/db.php';
//include '../includes/funciones.php';

$sql = "SELECT * FROM reservas_tours GROUP BY fecha_tour ORDER BY fecha_tour asc";
$q = mysqli_query($conexion, $sql);
if($q){
    while($data = mysqli_fetch_object($q)){
        $tours_historial[] = $data;
    }
    $contador1 = 0;
    foreach($tours_historial as $tour_historial){
        $fecha_tour = $tour_historial->fecha_tour;
        $datos[$contador1]['fecha'] = $fecha_tour;
        $sql = "SELECT reservas_tours.*, thorarios_tours.*, thorarios.*, ttours.*
        FROM reservas_tours 
        JOIN thorarios_tours ON thorarios_tours.id_horarios_tours = reservas_tours.id_horarios_tour
        JOIN thorarios ON thorarios.id_horario = thorarios_tours.id_horario
        JOIN ttours ON ttours.id_tour = thorarios_tours.id_tour
        WHERE fecha_tour = '$fecha_tour' 
        ORDER BY reservas_tours.id_horarios_tour asc";
        $q = mysqli_query($conexion, $sql);
        while($data = mysqli_fetch_object($q)){
            $tours[] = $data;
        }
        $contador2 = 0;
        foreach($tours as $tour){
            $datos[$contador1]["datos"][$contador2]["id_reserva_tour"] = utf8_encode($tour->id_reserva_tour);
            $datos[$contador1]["datos"][$contador2]["id_horarios_tour"] = utf8_encode($tour->id_horarios_tour);
            $datos[$contador1]["datos"][$contador2]["fecha_tour"] = utf8_encode($tour->fecha_tour);
            $datos[$contador1]["datos"][$contador2]["fecha_reserva"] = utf8_encode($tour->fecha_reserva);
            $datos[$contador1]["datos"][$contador2]["lugares_totales"] = utf8_encode($tour->lugares_totales);
            $datos[$contador1]["datos"][$contador2]["lugares_disponibles"] = utf8_encode($tour->lugares_disponibles);
            $datos[$contador1]["datos"][$contador2]["lugares_ocupados"] = utf8_encode($tour->lugares_ocupados);
            $datos[$contador1]["datos"][$contador2]["tour"] = utf8_encode($tour->nombre_tour);
            $datos[$contador1]["datos"][$contador2]["horario"] = utf8_encode($tour->horario);
            $contador2++;
        }
        unset($tours);
        $contador1++;
    }
    $json = json_encode($datos);
    echo $json ;
 }
 else{
    $datos["mensaje"]="Error al obtener los datos, intente de nuevo.";
    echo json_encode($datos);
 }
 mysqli_close($conexion);
?>


















