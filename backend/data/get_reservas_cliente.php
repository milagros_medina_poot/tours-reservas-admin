<?php
header("Content-Type: text/html;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");

include '../includes/db.php';
//include '../includes/funciones.php';
$json = file_get_contents('php://input'); 
$obj = json_decode($json,true);
$id_horarios_tour = intval($obj['id_horarios_tour']);
$fecha_tour = $obj['fecha_tour'];

$sql = "SELECT reservas_cliente.*, thorarios_tours.*, thorarios.*, ttours.* 
FROM reservas_cliente
JOIN thorarios_tours ON thorarios_tours.id_horarios_tours = reservas_cliente.id_horarios_tour
JOIN thorarios ON thorarios.id_horario = thorarios_tours.id_horario
JOIN ttours ON ttours.id_tour = thorarios_tours.id_tour
WHERE id_horarios_tour = $id_horarios_tour AND fecha_tour = '$fecha_tour'
ORDER BY reservas_cliente.id_horarios_tour asc";

$q = mysqli_query($conexion, $sql);

if($q){
   while($data = mysqli_fetch_object($q)){
       $reservas_cliente[] = $data;
   }
   $contador = 0;
   foreach($reservas_cliente as $reserva){
      $datos[$contador]["id_reserva_cliente"] = utf8_encode($reserva->id_reserva_cliente);
      $datos[$contador]["id_horarios_tour"] = utf8_encode($reserva->id_horarios_tour);
      $datos[$contador]["id_tour"] = utf8_encode($reserva->id_tour);
      $datos[$contador]["fecha_tour"] = utf8_encode($reserva->fecha_tour);
      $datos[$contador]["id_hotel"] = utf8_encode($reserva->id_hotel);
      $datos[$contador]["lugares_reservados"] = utf8_encode($reserva->lugares_reservados);
      $datos[$contador]["nombre_cliente"] = utf8_encode($reserva->nombre_cliente);
      $datos[$contador]["telefono"] = utf8_encode($reserva->telefono);
      $datos[$contador]["correo_electronico"] = utf8_encode($reserva->correo_electronico);
      $datos[$contador]["codigo_reserva"] = utf8_encode($reserva->codigo_reserva);
      $datos[$contador]["monto_total"] = utf8_encode($reserva->monto_total);
      $datos[$contador]["horario"] = utf8_encode($reserva->horario);
      $datos[$contador]["nombre_tour"] = utf8_encode($reserva->nombre_tour);
      $contador++;
  }
   $json = json_encode($datos);
   echo $json ;
}
else{
   $datos["mensaje"]="Error al obtener los datos, intente de nuevo.";
   echo json_encode($datos);
}
mysqli_close($conexion);
?>
