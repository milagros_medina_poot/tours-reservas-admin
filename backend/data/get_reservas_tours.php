<?php
header("Content-Type: text/html;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");

include '../includes/db.php';
//include '../includes/funciones.php';
$json = file_get_contents('php://input'); 
$obj = json_decode($json,true);
$fecha = $obj['fecha'];

$sql = "SELECT reservas_tours.*, thorarios_tours.*, thorarios.*, ttours.*
FROM reservas_tours 
JOIN thorarios_tours ON thorarios_tours.id_horarios_tours = reservas_tours.id_horarios_tour
JOIN thorarios ON thorarios.id_horario = thorarios_tours.id_horario
JOIN ttours ON ttours.id_tour = thorarios_tours.id_tour
WHERE fecha_tour = '$fecha' 
ORDER BY reservas_tours.id_horarios_tour asc";

$q = mysqli_query($conexion, $sql);

if($q){
   while($data = mysqli_fetch_object($q)){
       $tours_reservas[] = $data;
   }
   $contador = 0;
   foreach($tours_reservas as $tour){
      $datos[$contador]["id_reserva_tour"] = utf8_encode($tour->id_reserva_tour);
      $datos[$contador]["id_horarios_tour"] = utf8_encode($tour->id_horarios_tour);
      $datos[$contador]["fecha_tour"] = utf8_encode($tour->fecha_tour);
      $datos[$contador]["fecha_reserva"] = utf8_encode($tour->fecha_reserva);
      $datos[$contador]["lugares_totales"] = utf8_encode($tour->lugares_totales);
      $datos[$contador]["lugares_disponibles"] = utf8_encode($tour->lugares_disponibles);
      $datos[$contador]["lugares_ocupados"] = utf8_encode($tour->lugares_ocupados);
      $datos[$contador]["tour"] = utf8_encode($tour->nombre_tour);
      $datos[$contador]["horario"] = utf8_encode($tour->horario);
      $contador++;
  }
   $json = json_encode($datos);
   echo $json ;
}
else{
   $datos["mensaje"]="Error al obtener los datos, intente de nuevo.";
   echo json_encode($datos);
}
mysqli_close($conexion);
?>
