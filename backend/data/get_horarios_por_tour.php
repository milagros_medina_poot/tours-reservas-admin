<?php
header("Content-Type: text/html;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");

include '../includes/db.php';
//include '../includes/funciones.php';
$json = file_get_contents('php://input'); 
$obj = json_decode($json,true);
$id_tour = intval($obj['id_tour']);

$sql = "SELECT thorarios_tours.*, thorarios.* 
FROM thorarios_tours
JOIN thorarios ON thorarios.id_horario = thorarios_tours.id_horario
WHERE id_tour = $id_tour";

$q = mysqli_query($conexion, $sql);

if($q){
   while($data = mysqli_fetch_object($q)){
       $horarios_tours[] = $data;
   }
   $contador = 0;
   foreach($horarios_tours as $horario){
      $datos[$contador]["id_horarios_tours"] = utf8_encode($horario->id_horarios_tours);
      $datos[$contador]["id_tour"] = utf8_encode($horario->id_tour);
      $datos[$contador]["id_horario"] = utf8_encode($horario->id_horario);
      $datos[$contador]["horario"] = utf8_encode($horario->horario);
      $contador++;
  }
   $json = json_encode($datos);
   echo $json ;
}
else{
   $datos["mensaje"]="Error al obtener los datos, intente de nuevo.";
   echo json_encode($datos);
}
mysqli_close($conexion);
?>
