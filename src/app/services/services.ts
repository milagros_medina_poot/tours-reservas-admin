import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpHeaders, HttpRequest, HttpBackend} from '@angular/common/http';
import {environment} from '../../environments/environment.prod';

@Injectable()
export class ApiService {
  private url = ""

  constructor(public http: HttpClient) {
    this.url = `${environment.api}`;
  }

  public loginByEmail(correo:string, password:string){
    return this.http.post(this.url + 'ac/admin/login.php', {correo:correo, password:password})
  }

  public getHistorial(){
    return this.http.get(this.url + 'data/admin/get_historial_reservas.php')
  }

  public getReservasDelDía(){
    return this.http.get(this.url + 'data/admin/get_reservas_tours.php')
  }

  public getReservasProximas(){
    return this.http.get(this.url + 'data/admin/get_reservas_proximas.php')
  }

  public getReservasCliente(id_horarios_tour:string, fecha_tour:string){
    return this.http.post(this.url + 'data/admin/get_reservas_cliente.php', {id_horarios_tour:id_horarios_tour, fecha_tour:fecha_tour})
  }

  public getHorariosByTour(id_tour:string, id_horarios_tour:string){
    return this.http.post(this.url + 'data/admin/get_horarios_por_tour.php', {id_tour:id_tour, id_horarios_tour:id_horarios_tour})
  }

  public updateHorarioCliente(id_horarios_tour_nuevo:string, id_horarios_tour_ant:string, id_reserva_cliente:string, 
    id_reserva_tour:string, fecha_tour:string, lugares_reservados:string, id_vehiculo:string,
    tipo_tour:string){
      console.log(id_horarios_tour_ant)
    return this.http.post(this.url + 'ac/admin/update_horario_cliente.php', {id_horarios_tour_nuevo:id_horarios_tour_nuevo, 
      id_horarios_tour_ant:id_horarios_tour_ant, id_reserva_cliente:id_reserva_cliente, id_reserva_tour:id_reserva_tour, 
      fecha_tour:fecha_tour, lugares_reservados:lugares_reservados, id_vehiculo:id_vehiculo, tipo_tour:tipo_tour})
  }
  public getToken(){
    return this.http.get(this.url + 'data/admin/get_token.php')
  }
  public updateToken(){
    return this.http.get(this.url + 'ac/admin/change_token.php')
  }
  public cancelarReserva(id_horarios_tour:string, id_reserva_cliente:string, fecha_tour:string, lugares_reservados:string){
    console.log(id_horarios_tour, id_reserva_cliente, fecha_tour, lugares_reservados)
    return this.http.post(this.url + 'ac/admin/cancelar_reserva.php', 
    {id_horarios_tour:id_horarios_tour, id_reserva_cliente:id_reserva_cliente, fecha_tour:fecha_tour,
    lugares_reservados:lugares_reservados})
  }
  public getDataComisionesHoteles(){
    return this.http.get(this.url + 'data/admin/get_comisiones_hoteles.php')
  }
}
