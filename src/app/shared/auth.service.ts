import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }

  isLoggedIn(){
    if(localStorage.getItem('authToken')){
      this.loggedIn.next(true)
    }else{
      this.loggedIn.next(false)
    }
    return this.loggedIn.asObservable()
  }
}
