import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../services/services';
import { Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  correo: string = "";
  password: string = "";
  data: any
  logueando:boolean = false

  constructor(private apiServices:ApiService, private router:Router,
    private authService:AuthService) { }

  miFormulario = new FormGroup({
    correo: new FormControl("", [Validators.required, Validators.email]),
    password: new FormControl("", [Validators.required])
  })

  ngOnInit(): void {
    this.checkIfIsLoggedIn()
  }

  checkIfIsLoggedIn(){
    if(this.authService.isLoggedIn()){
      this.router.navigate(['reservas_del_dia'])
    }
  }

  login() {
    this.logueando = true
    this.correo = this.miFormulario.get("correo")?.value
    this.password = this.miFormulario.get("password")?.value

    console.log(this.correo);
    console.log(this.password);
    
    this.apiServices.loginByEmail(this.correo, this.password).subscribe(response=>{
      this.logueando = false
      this.data = response
      if(this.data.result == 1){
        localStorage.setItem('authToken', this.data.id_usuario)
        this.router.navigateByUrl('reservas_del_dia', { replaceUrl: true });
      } else {
        alert("Correo o contraseña incorrectos")
      }
    }, error=>{
      this.logueando = true
    });
  }

}
