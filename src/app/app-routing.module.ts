import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HistorialComponent } from './pages/historial/historial.component';
import { ReservasDelDiaComponent } from './pages/reservas-del-dia/reservas-del-dia.component';
import { ClientesByTourComponent } from './pages/clientes-by-tour/clientes-by-tour.component';
import { AuthGuard } from './shared/auth.guard';
import { TokenComponent } from './pages/token/token.component';
import { ReservasProximasComponent } from './pages/reservas-proximas/reservas-proximas.component';

const routes: Routes = [
  { path: "login", component:LoginComponent},
  { path: "historial", component:HistorialComponent, canActivate:[AuthGuard]},
  { path: "reservas_del_dia",component:ReservasDelDiaComponent,canActivate:[AuthGuard]},
  { path: "reservas_proximas",component:ReservasProximasComponent,canActivate:[AuthGuard]},
  { path: "token",component:TokenComponent, canActivate:[AuthGuard]},
  { path: "tour/:id_horarios_tour/:fecha_tour", component: ClientesByTourComponent, canActivate:[AuthGuard]},
  {path: '', redirectTo: '/reservas_del_dia', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
