import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {
  isLoggedIn$: Observable<boolean> | undefined;
  options:any = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
  fecha:Date = new Date()
  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit() {
    this.isLoggedIn$ = this.authService.isLoggedIn();
  }

  logout(snav:any){
    localStorage.removeItem('authToken')
    this.router.navigate(['login'])
    snav.toggle()
  }

}
