import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ApiService } from '../../services/services';

@Component({
  selector: 'app-token',
  templateUrl: './token.component.html',
  styleUrls: ['./token.component.css']
})
export class TokenComponent implements OnInit {
  public token:string = ""
  public respuestaToken:any = []
  public consultandoToken:boolean = false


  constructor(private apiServices:ApiService) { }

  ngOnInit(): void {
    this.getToken()
  }

  getToken(){
    this.consultandoToken = true
    this.apiServices.getToken().subscribe(result=>{
      this.consultandoToken = false
      this.respuestaToken = result
      if(this.respuestaToken.respuesta == '1'){
        this.token = this.respuestaToken.token
      }else{

      }
    }, error=>{
      this.consultandoToken = false
    });
  }
}
