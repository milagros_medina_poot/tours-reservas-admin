import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservasProximasComponent } from './reservas-proximas.component';

describe('ReservasProximasComponent', () => {
  let component: ReservasProximasComponent;
  let fixture: ComponentFixture<ReservasProximasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReservasProximasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservasProximasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
