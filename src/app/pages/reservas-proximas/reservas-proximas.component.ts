import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../services/services';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reservas-proximas',
  templateUrl: './reservas-proximas.component.html',
  styleUrls: ['./reservas-proximas.component.css']
})
export class ReservasProximasComponent implements OnInit {
  public reservas:any = []
  public dataSource:any = []
  public fecha:any
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public consultandoDatos:boolean = false
  public datePicker:any
  formatter = new Intl.NumberFormat('es-MX', {
    style: 'currency',
    currency: 'MXN',
  });

  displayedColumns: string[] = ['id_reserva', 'tour', 'horario', 'fecha_tour','lugares_ocupados', 'lugares_totales', 'total'];

  constructor(private apiServices:ApiService, private router:Router) { }

  ngOnInit(): void {
    this.getReservasProximas()
  }

  getReservasProximas(){
    this.consultandoDatos = true
    this.apiServices.getReservasProximas().subscribe(reservas=>{
      this.consultandoDatos = false
      this.reservas=reservas;
      this.dataSource = new MatTableDataSource(this.reservas);
      this.dataSource.paginator = this.paginator;
    }, error=>{
      this.consultandoDatos = false
    });
  }

  getClientesByTour(row:any){
    console.log(row)
    this.router.navigate(['/tour/'+row.id_horarios_tour+'/'+row.fecha_tour])
  }

}
