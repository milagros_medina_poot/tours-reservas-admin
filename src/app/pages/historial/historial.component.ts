import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from '../../services/services';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {
  public historial:any = []
  public dataSource:any = []
  public fecha:any
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public consultandoDatos:boolean = false
  public datePicker:any
  formatter = new Intl.NumberFormat('es-MX', {
    style: 'currency',
    currency: 'MXN',
  });
  displayedColumns: string[] = ['id_reserva', 'tour', 'horario', 'fecha_tour','lugares_ocupados', 'lugares_totales', 'total'];
  
  constructor(private apiServices:ApiService) { }

  ngOnInit(): void {
    this.getHistorialReservas()
  }

  getHistorialReservas(){
    this.consultandoDatos = true
    this.apiServices.getHistorial().subscribe(historial=>{
      this.consultandoDatos = false
      this.historial=historial;
      this.dataSource = new MatTableDataSource(this.historial);
      this.dataSource.paginator = this.paginator;
    }, error=>{
      this.consultandoDatos = false
    });
  }

  filtrarHistorialPorFecha(event: MatDatepickerInputEvent<Date>) {
    this.datePicker = event.value?.getFullYear()
    if((event.value?.getMonth()!)<9){
      this.datePicker += "-0" + (event.value?.getMonth()! + 1)
    } else{
      this.datePicker += "-" + (event.value?.getMonth()! + 1)
    }

    if((event.value?.getDate()!)<10){
      this.datePicker += "-0" + event.value?.getDate()
    } else{
      this.datePicker += "-" + event.value?.getDate()
    }

    let filter = this.historial.filter((item: { fecha_tour: any; }) => item.fecha_tour === this.datePicker)

    this.dataSource = filter
  }

  removerFiltro(fechaFiltro:any){
    this.dataSource = this.historial
    fechaFiltro.value = ""
  }

}
