import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ApiService } from '../../services/services';
import { DialogConfirmCancelReserva } from '../clientes-by-tour/clientes-by-tour.component';

@Component({
  selector: 'app-reservas-del-dia',
  templateUrl: './reservas-del-dia.component.html',
  styleUrls: ['./reservas-del-dia.component.css']
})
export class ReservasDelDiaComponent implements OnInit {
  public reservas:any=[{}]
  public comisiones:any =[]
  public dataSource:any = []
  options:any = { year: 'numeric', month: 'numeric', day: 'numeric' };
  public fecha:Date = new Date();
  public consultandoDatos:boolean = false
  resultadoUpdateToken:any = []
  displayedColumns: string[] = ['tour', 'horario', 'fecha_tour', 'lugares_totales', 'lugares_ocupados','lugares_disponibles'];
  formatter = new Intl.NumberFormat('es-MX', {
    style: 'currency',
    currency: 'MXN',
  });
  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;
  
  constructor(private apiServices:ApiService, private router:Router,
    private _snackBar: MatSnackBar, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.checkIfUpdateToken()
    this.getReservasDelDia()
  }

  checkIfUpdateToken(){
    let fecha_guardada = localStorage.getItem('fecha_guardada');
    let fecha = this.fecha.toLocaleDateString("es-MX", this.options)
    if(String(fecha) != fecha_guardada){
      this.updateToken()
      this.saveDate()
    }
  }

  saveDate(){
    let fecha = this.fecha.toLocaleDateString("es-MX", this.options)
    localStorage.setItem('fecha_guardada', fecha)
    console.log(fecha)
  }

  updateToken(){
    this.apiServices.updateToken().subscribe(result=>{
      this.resultadoUpdateToken = result
      this.openSnackBar(this.resultadoUpdateToken.mensaje)
    }, error=>{

    })
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 2000
    });
  }

  getReservasDelDia(){
    this.consultandoDatos = true
    this.apiServices.getReservasDelDía().subscribe(reservas=>{
      this.consultandoDatos = false
      this.reservas = reservas
      this.dataSource = new MatTableDataSource(this.reservas.data);
      this.dataSource.paginator = this.paginator
    }, error=>{
      this.consultandoDatos = false
    });
  }

  getClientesByTour(row:any){
    console.log(row)
    this.router.navigate(['/tour/'+row.id_horarios_tour+'/'+row.fecha_tour])
  }
  showComisionesPorHotel(){
    this.dialog.open(DialogComisionesHoteles, {
      width: '60%',
      height: '60%'
    });
  }
}

@Component({
  selector: 'dialog-comisiones-hoteles',
  templateUrl: './dialog-comisiones-hoteles.html',
  styleUrls: ['./dialog-comisiones-hoteles.css']
})
export class DialogComisionesHoteles implements OnInit {
  dataComisiones:any = []
  public dataSource:any = []
  displayedColumns: string[] = ['hotel', 'total', 'comision'];
  consultandoInformacion:boolean = false
  formatter = new Intl.NumberFormat('es-MX', {
    style: 'currency',
    currency: 'MXN',
  });
  constructor(
    private apiServices:ApiService,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogComisionesHoteles>
  ) {
   
  }
  ngOnInit(): void {
    this.getComisionesPorHotel()
  }

  getComisionesPorHotel(){
    this.consultandoInformacion = true
    this.apiServices.getDataComisionesHoteles().subscribe(result=>{
      this.consultandoInformacion = false
      this.dataComisiones = result
      if(this.dataComisiones.respuesta == '0'){
        this.openSnackBar(this.dataComisiones.mensaje)
        this.dialogRef.close()
        return;
      }
      this.dataSource = result
    }, error=>{
      this.consultandoInformacion = false
    })
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 2000
    });
  }
}

