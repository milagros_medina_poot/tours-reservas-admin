import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservasDelDiaComponent } from './reservas-del-dia.component';

describe('ReservasDelDiaComponent', () => {
  let component: ReservasDelDiaComponent;
  let fixture: ComponentFixture<ReservasDelDiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReservasDelDiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservasDelDiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
