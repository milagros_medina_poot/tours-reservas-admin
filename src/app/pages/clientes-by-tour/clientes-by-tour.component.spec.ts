import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesByTourComponent } from './clientes-by-tour.component';

describe('ClientesByTourComponent', () => {
  let component: ClientesByTourComponent;
  let fixture: ComponentFixture<ClientesByTourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientesByTourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesByTourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
