import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ApiService } from '../../services/services';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-clientes-by-tour',
  templateUrl: './clientes-by-tour.component.html',
  styleUrls: ['./clientes-by-tour.component.css']
})
export class ClientesByTourComponent implements OnInit {
  id_horarios_tour:any
  fecha_tour:any
  public reservas:any = []
  public dataSource:any = []
  id_reserva_cliente:any
  public codigo_reserva_search:string = ""
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  nombre_tour:any
  horario:any
  public consultandoDatos:boolean = false
  formatter = new Intl.NumberFormat('es-MX', {
    style: 'currency',
    currency: 'MXN',
  });
  displayedColumns: string[] = [ 'nombre_cliente', 'telefono', 'correo_electronico', 'codigo_reserva', 'lugares_reservados', 'tipo_tour', 'monto_pagado', 'hotel', 'opciones'];

  constructor(private rutaActiva: ActivatedRoute, private apiServices:ApiService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.id_horarios_tour = this.rutaActiva.snapshot.params['id_horarios_tour']
    this.fecha_tour = this.rutaActiva.snapshot.params['fecha_tour']
    this.getReservasCliente()
  }

  getReservasCliente(){
    this.consultandoDatos = true
    this.apiServices.getReservasCliente(this.id_horarios_tour, this.fecha_tour).subscribe(reservas=>{
      this.consultandoDatos = false
      this.reservas=reservas;
      if(this.reservas.respuesta == '2'){
        history.back()
        return;
      }
      this.nombre_tour = this.reservas[0].nombre_tour
      this.horario = this.reservas[0].horario
      this.dataSource = new MatTableDataSource(this.reservas);
      this.dataSource.paginator = this.paginator;
    }, error=>{
      this.consultandoDatos = false
    });
  }

  cancelarReservaCliente(element:any){
    const dialogRef = this.dialog.open(DialogConfirmCancelReserva, {
      width: '30%',
      data:{id_reserva_cliente: element.id_reserva_cliente, id_horarios_tour: element.id_horarios_tour,
      fecha_tour: element.fecha_tour, lugares_reservados: element.lugares_reservados}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result == '1'){
        location.reload();
      } 
    });
  }

  openDialogModificarHorario(element:any){
    this.id_reserva_cliente = element.id_reserva_cliente
    const dialogRef = this.dialog.open(DialogModificarHorario, {
      width: '30%',
      data:{id_horarios_tour_ant: this.id_horarios_tour,id_reserva_cliente: this.id_reserva_cliente, id_tour: element.id_tour, 
        id_reserva_tour: element.id_reserva_tour, fecha_tour:element.fecha_tour,
      lugares_reservados:element.lugares_reservados, id_vehiculo:element.id_vehiculo,
      tipo_tour:element.tipo_tour}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result == '1'){
        location.reload();
      } 
    });
  }

  backToReservas(){
    history.back();
  }

  filtrarTabla(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  removerFiltro(){
    this.dataSource = this.reservas
  }

}


@Component({
  selector: 'dialog-modificar-horario',
  templateUrl: 'dialog-modificar-horario.html',
})
export class DialogModificarHorario implements OnInit {
  id_horarios_tour_nuevo:any
  id_horarios_tour_ant:any
  id_reserva_cliente:any
  id_reserva_tour:any
  id_tour:any
  fecha_tour:any
  lugares_reservados:any
  id_vehiculo:any
  tipo_tour:any
  public horarios:any = []
  public modificandoHorario:boolean = false
  respuesta:any = []
  constructor(
    public dialogRef: MatDialogRef<DialogModificarHorario>,
    private apiServices:ApiService,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.id_horarios_tour_ant = data.id_horarios_tour_ant
    this.id_reserva_cliente = data.id_reserva_cliente
    this.id_tour = data.id_tour
    this.id_reserva_tour = data.id_reserva_tour
    this.fecha_tour = data.fecha_tour
    this.lugares_reservados = data.lugares_reservados
    this.id_vehiculo = data.id_vehiculo
    this.tipo_tour = data.tipo_tour
  }
  ngOnInit(): void {
  this.getToursHorarios()
  }

  getToursHorarios(){
    this.apiServices.getHorariosByTour(this.id_tour, this.id_horarios_tour_ant).subscribe(horarios=>{
      this.horarios=horarios;
    });
  }

  modificarHorario(): void {
    this.modificandoHorario = true
    this.apiServices.updateHorarioCliente(this.id_horarios_tour_nuevo, this.id_horarios_tour_ant,
      this.id_reserva_cliente, this.id_reserva_tour, this.fecha_tour, this.lugares_reservados,
      this.id_vehiculo, this.tipo_tour).subscribe(result=>{
        this.modificandoHorario = false
        this.respuesta = result
        this.openSnackBar(this.respuesta.mensaje)
        if(this.respuesta.respuesta == '1'){
          this.dialogRef.close('1')
        }
    }, error=>{
      this.modificandoHorario = false
    })
  }
  openSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 2000
    });
  }
}


@Component({
  selector: 'dialog-confirm-cancel.reserva',
  templateUrl: 'dialog-confirm-cancel.reserva.html',
})
export class DialogConfirmCancelReserva implements OnInit {
  id_reserva_cliente:any
  id_horarios_tour:any
  fecha_tour:any
  lugares_reservados:any
  cancelandoReserva:boolean = false
  resultadoCancelacion:any = []
  constructor(
    private apiServices:ApiService,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogConfirmCancelReserva>
  ) {
   this.id_horarios_tour = data.id_horarios_tour
   this.id_reserva_cliente = data.id_reserva_cliente
   this.fecha_tour = data.fecha_tour
   this.lugares_reservados = data.lugares_reservados
  }
  ngOnInit(): void {
  }

  cancelarReserva(){
    this.cancelandoReserva = true
    this.apiServices.cancelarReserva(this.id_horarios_tour, this.id_reserva_cliente, 
      this.fecha_tour, this.lugares_reservados).subscribe(result=>{
        this.cancelandoReserva = false
        this.resultadoCancelacion = result
        console.log(result)
        if(this.resultadoCancelacion.respuesta == '1'){
          this.dialogRef.close('1')
        }
        this.openSnackBar(this.resultadoCancelacion.mensaje)
      }, error=>{
        this.cancelandoReserva = false
      })
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 2000
    });
  }
}

